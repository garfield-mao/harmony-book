import storage from '@system.storage';
import fetch from '@system.fetch';

// 定义存储类对象
class Storage {
    constructor() {
        // 搜索历史的key
        this.searchListKey = "searchList";
        this.userKey = "userKey"
    }

    set(key, value) {
        return new Promise((resolve, reject) => {
            storage.set({
                key: key,
                value: value,
                success: function () {
                    resolve();
                },
                fail: function (data, code) {
                    reject();
                },
            });
        });
    }

    get(key) {
        return new Promise((resolve, reject) => {
            storage.get({
                key: key,
                success: function (data) {
                    resolve(data)
                },
                fail: function (data, code) {
                    reject()
                },
                complete: function () {
                    console.info("===========store.get()=============")
                },
            });
        });
    }

    clear() {
        return new Promise((resolve, reject) => {
            storage.clear({
                success: function () {
                    resolve()
                },
                fail: function (data, code) {
                    reject()
                },
            });
        });
    }

    delete(key) {
        return new Promise((resolve, reject) => {
            storage.delete({
                key: key,
                success: function () {
                    resolve()
                },
                fail: function (data, code) {
                    reject()
                },
            });
        });
    }
}


const timeList = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
    let list = [year, month, day, hour, minute, second]
    return list
}

// time 评论发表时间(毫秒)
const formatTime = time => {

    let date = new Date(time)
    let list = timeList(date)


    let currentDate = new Date()
    let currentList = timeList(currentDate)

    let second = parseInt(time / 1000)
    let currentSecond = parseInt(currentDate.getTime() / 1000)
    // 单位:秒
    let distance = currentSecond - second

    if (distance > 0 && distance < 60) {
        return `${distance}秒前`
    }
    // 分
    distance = parseInt(distance / 60)

    if (distance > 0 && distance < 60) {
        return `${distance}分前`
    }

    // 小时
    distance = parseInt(distance / 60)

    if (distance > 0 && distance < 24) {
        return `${distance}小时前`
    }


    // 天
    distance = parseInt(distance / 24)

    if (distance > 0 && distance < 7) {
        return `${distance}天前`
    } else if (distance == 7) {
        return `1星期前`
    }

    if (distance > 365 && list[0] < currentList[0]) {
        return `${list[0]}-${list[1]}-${list[2]}`
    } else {
        return `${list[1]}-${list[2]}`
    }
}


const imgExchange = function (img) {
    if (img) {
        img = img.replace(/(\r\n)|(\n)|( )/g, '');
    }
   /* if (img && img.indexOf('yuewen') > 0) {
        if (img.indexOf("jpeg") < 0) {
            img = img + ".jpeg";
        }
    }*/
    return img
}


export default {
    store: new Storage(),
    formatTime: formatTime,
    imgExchange: imgExchange
}

