import router from '@system.router';
import prompt from '@system.prompt';
import util from '../../common/js/util.js';

export default {
    data: {
        result: null,
        index: 0,
        value: ""
    },
    onInit() {
        if (this.value)
        this.doSearch(this.value)
    },
    goLeft: function () {
        router.back()
    },
    search: function (e) {
        if (e.text) {
            this.doSearch(e.text);
        }
    },
    keyChange: function (e) {
        this.value = e.text
    },
    doSearch: function (text) {

        let that = this
        this.$app.$def.http({
            url: `/search?key=${text}&n=30`,
            method: "POST"
        }).then(res => {
            if (res.status == 200) {
                let result = [{
                                  title: "全部",
                                  list: [],
                                  field: 'all'
                              }, {
                                  title: "书名",
                                  list: [],
                                  field: 'name'
                              }, {
                                  title: "作者",
                                  list: [],
                                  field: 'author'
                              }, {
                                  title: "简介",
                                  list: [],
                                  field: 'synopsis',
                              }]

                res.data.forEach(book => {
                    // console.info(JSON.stringify(book))

                    for (let i = 1;i < result.length; i++) {

                        // console.info(book[result[i].field])

                        if (book[result[i].field].search('span') != -1) {
                            // sdk5.0 没有富文本标签,等到6.0后删除这行代码,加上<richtext>即可,  richtext组件不好用，效率低
                            book[result[i].field] = book[result[i].field].replace(/<[^<>]+?>/g, '').replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) {
                                let arrEntities = {
                                    'lt': '<',
                                    'gt': '>',
                                    'nbsp': ' ',
                                    'amp': '&',
                                    'quot': '"'
                                };
                                return arrEntities[t];
                            });

                            result[i].list.push(book);
                        }

                    }

                    result[0].list.push(book)
                })

                that.result = result
            } else {
                prompt.showToast({
                    message: "再试一次",
                    duration: 2000
                });
            }
        }).catch(error => {
            prompt.showToast({
                message: "网络故障",
                duration: 2000
            });
        });

        // 保存搜索记录
        this.saveHistory(text)

    },
    saveHistory: function (value) {
        util.store.get(util.store.searchListKey).then(data => {
            if (data) {
                data = JSON.parse(data)
                let searchList = data

                // 搜索关键词存入搜索历史中
                let t = -1;
                for (let i = 0;i < searchList.length; i++) {
                    if (value == searchList[i]) {
                        t = i;
                        break;
                    }
                }
                // 删除重复元素
                if (t > -1) {
                    if (searchList.length == 1) {
                        searchList = []
                    } else {
                        searchList.splice(t, 1)
                    }
                }

                searchList.unshift(value)
                util.store.set(util.store.searchListKey, JSON.stringify(searchList))

            } else {
                util.store.set(util.store.searchListKey, JSON.stringify([value]))
            }
        })
    },
    tabChange: function (e) {
        // this.doSearch(this.value, e.index);
    }
}
