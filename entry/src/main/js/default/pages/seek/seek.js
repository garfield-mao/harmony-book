import router from '@system.router';
import util from '../../common/js/util.js';

export default {
    data: {
        value: "",
    // 搜索历史
        searchList: []
    },
    onInit() {
        let that = this
        // 获取搜索历史
        util.store.get(util.store.searchListKey).then(data => {
            if (data) {
                data = JSON.parse(data)
                let t = []
                that.searchList = t.concat([], data)
            }
        })
    },  onShow() {
        let that = this
        // 获取搜索历史
        util.store.get(util.store.searchListKey).then(data => {
            if (data) {
                data = JSON.parse(data)
                let t = []
                that.searchList = t.concat([], data)
            }
        })
    },
    goLeft: function () {
        router.back()
    },
    search: function (e) {

        router.push({
            uri: "pages/explore/explore",
            params: {
                value: this.value
            }
        })
    },
    saveHistory: function (searchList, value) {
        // 搜索关键词存入搜索历史中
        let t = -1;
        for (let i = 0;i < searchList.length; i++) {
            if (value == searchList[i]) {
                t = i;
            }
        }
        if (t > 0) {
            searchList.splice(t, 1)
        }

        searchList.unshift(value)

        util.store.set(util.store.searchListKey, JSON.stringify(searchList))
    },
    goExplore: function (value) {
        //        this.saveHistory(this.searchList, value)

        router.push({
            uri: "pages/explore/explore",
            params: {
                value: value
            }
        })
    },
    keyChange: function (e) {
        this.value = e.text
    },
    close: function (i) {
        this.searchList.splice(i, 1)
        util.store.set(util.store.searchListKey, JSON.stringify(this.searchList))
    }
}
