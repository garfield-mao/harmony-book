import router from '@system.router';

export default {
    data: {
        shelf: []
    },
    onInit() {

    },
    onShow() {
        this.onShelfRefresh()
    },
    onShelfRefresh: function () {
        let that = this;
        if (this.$app.$def.globalData.token) {
            this.$app.$def.http({
                url: "/shelf/profileShelf"
            }).then(res => {
                if (res.status == 200) {
                    let temp = [];
                    that.shelf = temp.concat(temp, res.data)
                }
            });
        }
    },
    goContents: function (id) {
        router.push({
            uri: "pages/contents/contents",
            params: {
                id: id
            }
        })
    }
}
