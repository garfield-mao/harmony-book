import util from '../../common/js/util.js';
import router from '@system.router';
import prompt from '@system.prompt';

export default {
    data: {
        username: "",
        password: ""
    },
    login: function () {
        let that = this
        this.$app.$def.http({
            url: `/login/in`,
            data: {
                username: this.username,
                password: this.password
            }
        }).then(res => {
            if (res.status == 200) {
                util.store.set(util.store.userKey, JSON.stringify({
                    username: that.username,
                    password: that.password
                }))
                that.$app.$def.globalData.token = res.data
                router.back()
            } else {
                prompt.showToast({
                    message: "登录失败"
                })
            }
        }).catch(e => {
            prompt.showToast({
                message: "登录失败"
            })
        })
    },
    change1: function (e) {
        this.username = e.value
    },
    change2: function (e) {
        this.password = e.value
    }
}
