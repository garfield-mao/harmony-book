import prompt from '@system.prompt';

export default {
    data: {
        title: '',
        contentsId: 0,
        webId: 0,
        bookId: 0,
        url: "",
        // 阅读章节缓存列表
        chapterList: [],
        //进去加载
        loading: false,
        // 底部栏加载
        bottomLoading: false,
        //刷新加载
        isRefreshing: false,
        isOpen: false,
        // 章节列表
        list: [],
        // 章节列表顺序,true为正序,false为倒序
        order: true,
        // 刷新章节目录状态
        isRefresh: false
    },
    onInit() {
        this.chapter(this.url, false)
    },
    // 查看章节目录
    lookContents() {
        // 先关闭设置面板
        this.closePanel('simplepanel');
        this.showPanel('chapters_panel')
        if (this.list.length == 0) {
            this.getList()
        }
    },
    chapterPanelSizeChange(e) {
        if (e.mode == "mini" || e.mode == "half") {
            this.closePanel('chapters_panel')
        }
    },
    reverse() {
        this.list.reverse()
    },

    getList() {
        this.isRefresh = true

        this.$app.$def.http({
            url: `/contents/vo/${this.bookId}/${this.webId}`
        }).then(res => {
            this.isRefresh = false

            if (res.status == 200) {
                let temp = []
                temp = temp.concat(res.data)
                if (!this.order) {
                    temp.reverse()
                }
                this.list = temp
            } else {
                prompt.showToast({
                    message: "刷新重试",
                    duration: 2000
                });
            }
        }).catch(error => {
            this.isRefresh = false

            prompt.showToast({
                message: "网络错误",
                duration: 2000
            });
        });
    },


    panelSizeChange(e) {
        if (e.mode == "mini") {
            this.closePanel('simplepanel')
        }
    },
    panelChange() {
        if (this.isOpen) {
            this.closePanel('simplepanel');
            this.isOpen = false
        } else {
            this.showPanel('simplepanel');
            this.isOpen = true
        }
    },
    showPanel(pannelId) {
        this.$element(pannelId).show()
    },
    closePanel(pannelId) {
        this.$element(pannelId).close()
    },
    refresh: function () {
        this.chapter(this.url, false)
    },
    next: function () {
        let url = this.chapterList[this.chapterList.length - 1].nextUrl
        if (url == null || url == "") {
            prompt.showToast({
                message: "已是最新章节"
            });
            return;
        }

        if (this.bottomLoading) {
            return
        }
        this.chapter(url, false)
    },
    toNext: function () {
        let url = this.chapterList[this.chapterList.length - 1].nextUrl
        if (!url || url == "") {
            prompt.showToast({
                message: "已是最新章节"
            });
            return;
        }
        this.chapter(url, true)
    },
    previous: function () {
        let url = this.chapterList[0].previousUrl

        if (!url || url == "") {
            prompt.showToast({
                message: "没有上一章"
            });
            return;
        }

        if (this.isRefreshing) {
            return
        }

        this.chapter(url, true)
    },

    goChapter(e) {
        let url = e.target.dataSet.url;
        this.closePanel("chapters_panel")
        this.chapter(url, true)
        //  this.showPanel("simplepanel")
    },

    chapter: function (url, isPrevious) {

        // 进入页面初始化
        if (url == this.url) {
            this.loading = true
        }

        if (!isPrevious) {
            // 下一章
            this.bottomLoading = true
        } else {
            // 上一章
            this.isRefreshing = true
        }

        let that = this
        this.$app.$def.http({
            url: `/chapter/vo?url=${url}&contentsId=${this.contentsId}&webId=${this.webId}&bookId=${this.bookId}`,
        }).then(res => {

            if (isPrevious) {
                that.isRefreshing = false
            }


            if (that.bottomLoading) {
                that.bottomLoading = false
            }

            if (that.loading) {
                that.loading = false
            }

            // console.info(JSON.stringify(res))
            if (res.status == 200 && res.data) {
                if (!res.data.title) {
                    prompt.showToast({
                        message: "没有了"
                    });
                    return;
                }


                this.title = res.data.title
                // 处理获取的content，因为api5不支持富文本 <rich-text>标签，所以要将content处理为text
                res.data.content = res.data.content.replace(/<[^<>]+?>/g, '').replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) {
                    let arrEntities = {
                        'lt': '<',
                        'gt': '>',
                        'nbsp': ' ',
                        'amp': '&',
                        'quot': '"'
                    };
                    return arrEntities[t];
                });

                // 是否为上一章
                if (isPrevious) {
                    let temp = []
                    temp.push(res.data)
                    // that.chapterList.push(res.data)
                    // that.chapterList = temp.concat(res.data, that.chapterList)
                    that.chapterList = temp
                }
                else {
                    let temp = []
                    // that.chapterList.push(res.data)
                    that.chapterList = temp.concat(that.chapterList, res.data)
                }

            } else {
                prompt.showToast({
                    message: "再试一次"
                });
            }
        }).catch(error => {

            if (isPrevious) {
                that.isRefreshing = false
            }


            if (that.bottomLoading) {
                that.bottomLoading = false
            }

            if (that.loading) {
                that.loading = false
            }
        });
    }
}
