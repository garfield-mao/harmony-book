import prompt from '@system.prompt';
import router from '@system.router';
import util from '../../common/js/util.js';

export default {
    data: {
        //我的
        //书架
        shelf: [],
        //首页
        tabIndex: 0,
        // 推荐
        recommends: [],
        pageSize: 20,
        // 起点
        qidian: [],
        zongheng: [],
        k17: [],
        tools: [{
                    icon: "/common/images/toolbar/home.png",
                    title: "首页",
                    selectIcon: "/common/images/toolbar/s_home.png"
                }, {
                    icon: "/common/images/toolbar/user.png",
                    title: "我的",
                    selectIcon: "/common/images/toolbar/s_user.png"
                }],
        // 我的 所在索引
        profileToolIndex: 1,
        // tool-bar的当前索引
        currentToolIndex: 0,
        // 是否已经登录
        isLogin: false,
        // loading

        isLoading: false,
        isLoading1: false,
        isLoading2: false,
        isLoading3: false
    },
    // tool-bar 改变时的函数
    toolChange: function (index) {
        this.currentToolIndex = index
        // 我的
        if (this.currentToolIndex == this.profileToolIndex) {
            if (this.$app.$def.globalData.token) {
                this.isLogin = true
                this.onShelfRefresh()
            } else {
                this.isLogin = false
                this.shelf = []
            }
        }
    },
    goShelf: function () {
        router.push({
            uri: "pages/shelf/shelf"
        })
    },
    onShow() {
        // 我的
        if (this.currentToolIndex == this.profileToolIndex) {
            if (this.$app.$def.globalData.token) {
                this.isLogin = true
                this.onShelfRefresh()
            } else {
                this.isLogin = false
                this.shelf = []
            }
        }
    },
    goLogin: function () {
        router.push({
            uri: "pages/login/login"
        })
    },
    onInit() {
        console.info('=================index===================')

        this.onRecommendsRefresh()

    },
    onShelfRefresh: function () {
        let that = this;
        this.$app.$def.http({
            url: "/shelf/profileShelf"
        }).then(res => {
            if (res.status == 200) {
                let temp = [];
                that.shelf = temp.concat(temp, res.data)
            }
        });
    },
    recommendsInit: function () {

        if (this.isLoading) {
            return
        }

        this.isLoading = true

        let that = this
        let start = parseInt(Math.random() * 1000000) % 390000;
        let pageSize = that.pageSize
        this.$app.$def.http({
            url: `/book/all?start=${start}&pageSize=${pageSize}`
        }).then(res => {

            that.isLoading = false

            if (res.status == 200) {
                let temp = [];
                if (that.recommends.length > that.pageSize * 2 - 1) {
                    that.recommends = temp.concat(res.data.list, that.recommends.splice(0, that.pageSize));
                } else {
                    that.recommends = temp.concat(res.data.list, that.recommends);
                }
            }

        }).catch(error => {
            that.isLoading = false
        });
    },
    onRecommendsRefresh: function (e) {
        this.recommendsInit()
    },
    qdInit: function () {
        if (this.isLoading1) {
            return
        }
        this.isLoading1 = true

        let that = this;
        let start = parseInt(Math.random() * 80000);
        let pageSize = that.pageSize
        this.$app.$def.http({
            url: `/book/all?start=${start}&pageSize=${pageSize}&sourceSite=起点中文网`
        }).then(res => {
            that.isLoading1 = false

            if (res.status == 200) {
                let temp = [];
                if (that.qidian.length > that.pageSize * 2 - 1) {
                    that.qidian = temp.concat(res.data.list, that.qidian.splice(0, that.pageSize))
                } else {
                    that.qidian = temp.concat(res.data.list, that.qidian)
                }

            }
        }).catch(error => {
            that.isLoading1 = false
        })
    },
    onQDRefresh: function (e) {
        this.qdInit();
    },
    zhInit: function () {
        if (this.isLoading2) {
            return
        }
        this.isLoading2 = true

        let that = this;
        let start = parseInt(Math.random() * 100000)
        let pageSize = that.pageSize
        this.$app.$def.http({
            url: `/book/all?start=${start}&pageSize=${pageSize}&sourceSite=纵横中文网`
        }).then(res => {
            that.isLoading2 = false

            if (res.status == 200) {
                let temp = [];
                if (that.zongheng.length > that.pageSize * 2 - 1) {
                    that.zongheng = temp.concat(res.data.list, that.zongheng.splice(0, that.pageSize));
                } else {
                    that.zongheng = temp.concat(res.data.list, that.zongheng);
                }
            }
        }).catch(error => {
            that.isLoading2 = false
        });
    },
    onZHRefresh: function (e) {
        this.zhInit();
    },
    k17Init: function () {
        if (this.isLoading3) {
            return
        }
        this.isLoading3 = true

        let that = this;
        let start = parseInt(Math.random() * 200000);
        let pageSize = that.pageSize
        this.$app.$def.http({
            url: `/book/all?start=${start}&pageSize=${pageSize}&sourceSite=17K小说网`
        }).then(res => {
            that.isLoading3 = false

            if (res.status == 200) {
                let temp = [];
                if (that.k17.length > that.pageSize * 2 - 1) {
                    that.k17 = temp.concat(res.data.list, that.k17.splice(0, that.pageSize))
                } else {
                    that.k17 = temp.concat(res.data.list, that.k17)
                }
            }

        }).catch(error => {
            that.isLoading3 = false
        });
    },
    onK17Refresh: function (e) {
        this.k17Init()
    },
    change: function (e) {
        this.tabIndex = e.index
        if (this.tabIndex == 0 && this.recommends.length == 0) {
            this.recommendsInit();
        } else if (this.tabIndex == 1 && this.qidian.length == 0) {
            this.qdInit();
        } else if (this.tabIndex == 2 && this.zongheng.length == 0) {
            this.zhInit();
        } else if (this.tabIndex == 3 && this.k17.length == 0) {
            this.k17Init();
        }
    },
    goSearch: function () {
        router.push({
            uri: "pages/seek/seek"
        })
    },
    goContents: function (id) {
        router.push({
            uri: "pages/contents/contents",
            params: {
                id: id
            }
        })
    },
    // 起点中文网图片需要处理才能显示出来
    imgExchange: function (img) {
        return util.imgExchange(img)
    }
}
