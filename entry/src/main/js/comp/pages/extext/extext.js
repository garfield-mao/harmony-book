export default {
    data: {
        isExtensible: false
    },
    props: {
        title: {
            default: ""
        },
        lines: {
            default: 3
        },
        size: {
            default: 12
        }
    },
    change() {
        this.isExtensible = !this.isExtensible
    }
}
