export default {
    data: {
        globalX: -1,
        globalY: -1
    },
    props: {
        top: {
            default: 200
        },
        left: {
            default: 200
        }
    },
    move: function (e) {
        console.warn("================组件移动================")
        console.warn(JSON.stringify(e))

        let touch = e.touches[0];
        let changedTouch = e.changedTouches[0];

        if (this.globalX == -1 && this.globalY == -1) {
            this.globalX = changedTouch.globalX
            this.globalY = changedTouch.globalY
        } else {
            let moveX = changedTouch.globalX - this.globalX;
            let moveY = changedTouch.globalY - this.globalY;

            this.globalX = changedTouch.globalX
            this.globalY = changedTouch.globalY

            this.top = this.top + moveY;
            this.left = this.left + moveX;

            console.warn("top: " + this.top + "-- left: " + this.left)
        }
    },
    start: function (e) {
        console.warn("================组件start================")
        console.warn(JSON.stringify(e))


    },
    go: function () {

    },
    end: function (e) {
        console.warn("================组件end================")
        console.warn(JSON.stringify(e))
        this.globalX = -1
        this.globalY = -1
    }
}
