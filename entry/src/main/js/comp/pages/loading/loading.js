// @ts-nocheck
import device from '@system.device';

export default {
    data: {},
    props: {
        isShow: {
            default: true
        },
        bottom: {
            default: 0
        },
        right: {
            default: 0
        },
        width: {
            default: 50
        },
        height: {
            default: 50
        },
        isPosition: {
            default: true
        },
        type: {
            default: "page" // page 页面加载 (页面中间);  bottom 底部加载;
        }
    },
    onInit() {
        let that = this
        device.getInfo({
            success: function (data) {
                if (that.right == 0) {
                    that.right = (data.windowWidth - that.width) / data.screenDensity
                }
                if (that.bottom == 0) {
                    that.bottom = (data.windowHeight - that.height) / data.screenDensity
                }

            },
            fail: function (data, code) {
            },
        });
    }
}
