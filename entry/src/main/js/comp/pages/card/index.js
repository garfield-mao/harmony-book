import router from '@system.router';
import util from '../../../default/common/js/util.js';

export default {
    data() {
        return {
            rich: ""
        };
    },
    props: {
        width: {
            default: 75
        },
        height: {
            default: 100
        },
        img: {
            default: ""
        },
        name: {
            default: ""
        },
        author: {
            default: ""
        },
        state: {
            default: ""
        },
        size: {
            default: 15
        },
        sourceSite: {
            default: ""
        },
        url: {
            default: ""
        },
        isRich: {
            default: false
        }
    },
    onInit() {
       /* this.rich = `<text  class="title text" style="font-size : ${this.size} px;">${this.name}</text>
            <text  class="author text" style="font-size : ${this.size} px;">${this.author}</text>
            <text  class="state text" style="font-size : ${this.size} px;">${this.state}</text>`
            */
    },
    onPageShow() {

    },
    // 页面跳转函数
    go: function () {
        if (this.url && this.url != "") {
            let route = this.parseUrl(this.url);
            router.push(route);
        }
    },
    /*解析父组件参数url*/
    parseUrl: function (url) {
        let route = {};
        // 有参数
        if (url.indexOf("?", 0) != -1) {
            let temp = url.split("?");
            route.uri = temp[0];
            route.params = {};
            if (temp[1].indexOf("&", 0) != -1) {
                let params = temp[1].split("&");
                for (let i = 0;i < params.length; i++) {
                    let param = params[i];
                    let map = param.split("=");
                    route.params[map[0]] = map[1];
                }
                return route;
            } else {
                let map = temp[1].split("=");
                route.params = {};
                route.params[map[0]] = map[1];
                return route;
            }

        }
        // 无参数
        else {
            route.uri = url;
            return route;
        }
    },
    // 图片加载成功回调函数
    imgComplete: function (rich) {
    },
    imgError: function (rich) {
    },
    computed: {
        image() {
            let img = util.imgExchange(this.img)
            // console.info(img)
            return img
        }
    }
}
