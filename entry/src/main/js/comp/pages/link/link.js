import router from '@system.router';

/**
 * 跳转章节链接
 *
 * 没有其他作用,只是因为 API5 无法使用 data-*
 * 从而通过组件来跳转
 */
export default {
    data() {
        return {}
    },
    props: {
        title: {
            default: ""
        },
        webId: {
            default: 0
        },
        contentsId: {
            default: 0
        },
        bookId: {
            default: 0
        },
        url: {
            default: ""
        }
    },
    go: function () {
        router.push({
            uri: "pages/chapter/chapter",
            params: {
                url: this.url,
                contentsId: this.contentsId,
                webId: this.webId,
                bookId: this.bookId
            }
        })
    }
}
