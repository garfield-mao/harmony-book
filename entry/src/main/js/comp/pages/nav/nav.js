import router from '@system.router';

export default {
    data() {
        return {}
    },
    props: {
        title: {
            default: "标题"
        }
    },
    goHome: function () {
        router.push({
            uri: "/"
        })
    },
    goLeft: function () {
        router.back();
    }
}
